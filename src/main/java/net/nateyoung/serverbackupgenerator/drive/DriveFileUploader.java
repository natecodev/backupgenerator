package net.nateyoung.serverbackupgenerator.drive;


import com.google.api.client.http.FileContent;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;

import java.io.IOException;
import java.util.Collections;

public class DriveFileUploader {

    public static File upload(java.io.File fileToUpload, Drive driveService, String location) throws IOException {

        File fileMetadata = new File();
        fileMetadata.setName(fileToUpload.getName());
        fileMetadata.setParents(Collections.singletonList(location));
        java.io.File filePath = new java.io.File(fileToUpload.getName());
        FileContent mediaContent = new FileContent("application/tar", fileToUpload.getAbsoluteFile());
        File file = driveService.files().create(fileMetadata, mediaContent)
                .setFields("id, parents")
                .execute();
        return file;
    }


}
