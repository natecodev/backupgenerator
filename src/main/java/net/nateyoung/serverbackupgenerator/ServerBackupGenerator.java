package net.nateyoung.serverbackupgenerator;

import com.google.api.services.drive.Drive;
import net.nateyoung.serverbackupgenerator.drive.DriveBoilerPlate;
import net.nateyoung.serverbackupgenerator.drive.DriveFileUploader;
import net.nateyoung.serverbackupgenerator.file.FileArchiver;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ServerBackupGenerator {

    private static final DriveBoilerPlate driveBoilerPlate = new DriveBoilerPlate();

    //arguments: <directory> <folder-id>
    public static void main(String[] args) throws IOException, Exception {
        Drive driveService = driveBoilerPlate.getDriveService();

        if (args.length < 2) {
            System.out.println("Not enough arguments.");
            return;
        }

        String fileToBackup = args[0].replace("*", " ");
        String googleDriveFolder = args[1];

        System.out.println("Backing up: " + fileToBackup);
        System.out.println("To drive folder: " + googleDriveFolder);

        String backupName = new SimpleDateFormat("yyyy_MM_dd_HHmm'.zip'").format(new Date());

        File directoryToBackup = new File(fileToBackup);

        if (directoryToBackup != null) {
            String archiveFilename = directoryToBackup.getName() + "-" + backupName;
            System.out.println("Creating archive file: " + archiveFilename);
            File tarFile = FileArchiver.zipDir(archiveFilename, directoryToBackup.getAbsolutePath());
            System.out.println("Created archive: " + tarFile.getName());

            System.out.println("Uploading archive to google drive in folder: " + googleDriveFolder);
            DriveFileUploader.upload(tarFile, driveService, googleDriveFolder);
            System.out.println("File uploaded to drive");
        } else {
            System.out.println("You did not enter a valid directory.");
        }

    }

}