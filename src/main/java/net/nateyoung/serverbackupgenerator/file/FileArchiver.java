package net.nateyoung.serverbackupgenerator.file;

import org.kamranzafar.jtar.TarEntry;
import org.kamranzafar.jtar.TarOutputStream;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class FileArchiver {

    /**
     * Take a file or directory input and turn it into an archive file
     *
     * @param File or directory to be archived
     * @return archive file
     */
    public static File createArchiveFile(File directoryToArchive, String pathName) throws IOException {

        if (directoryToArchive.isDirectory() == false) {
            System.out.println(directoryToArchive.getPath() + " is not a directory");
            return null;
        }

        File target = new File(pathName);

        // Output file stream
        FileOutputStream dest = new FileOutputStream( target );

        // Create a TarOutputStream
        TarOutputStream out = new TarOutputStream( new BufferedOutputStream( dest ) );

        // Files to tar
        File[] filesToTar=directoryToArchive.listFiles();

        for (int i=0; i<filesToTar.length; i++) {
            File f = filesToTar[i];

            out.putNextEntry(new TarEntry(f, f.getName()));
            BufferedInputStream origin = new BufferedInputStream(new FileInputStream( f ));

            int count;
            byte data[] = new byte[2048];
            while((count = origin.read(data)) != -1) {
                out.write(data, 0, count);
            }

            out.flush();
            origin.close();
        }

        out.close();

        return target;
    }

    public static File zipDir(String zipFileName, String dir) throws Exception {
        File dirObj = new File(dir);
        ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));
        System.out.println("Creating : " + zipFileName);
        addDir(dirObj, out);
        out.close();

        return new File(zipFileName);
    }

    static void addDir(File dirObj, ZipOutputStream out) throws IOException {
        File[] files = dirObj.listFiles();
        byte[] tmpBuf = new byte[1024];

        for (int i = 0; i < files.length; i++) {
            if (files[i].isDirectory()) {
                addDir(files[i], out);
                continue;
            }
            FileInputStream in = new FileInputStream(files[i].getAbsolutePath());
            System.out.println(" Adding: " + files[i].getAbsolutePath());
            out.putNextEntry(new ZipEntry(files[i].getAbsolutePath()));
            int len;
            while ((len = in.read(tmpBuf)) > 0) {
                out.write(tmpBuf, 0, len);
            }
            out.closeEntry();
            in.close();
        }
    }


}
